var path = require('path');
var pkg = require('./package.json');
var webpack = require('webpack');
var ngAnnotatePlugin = require('ng-annotate-webpack-plugin');

var env = process.env.NODE_ENV || 'development';

var plugins = [
    new ngAnnotatePlugin({  add: true }),
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify(env)
    })
];

if (env === 'production') {
    plugins.push(
      new webpack.optimize.OccurenceOrderPlugin(),
      new webpack.optimize.UglifyJsPlugin({
        compress: {
          warnings: false
        }
      })
    );
}

module.exports = {
    entry: {
        index: './client/js/core/app.js'
    },

    output: {
        filename: '[name]-' + pkg.version + '.js',
        path: path.resolve('client/dist/'),
        publicPath: '/dist/'
    },

    module: {
        loaders: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                loader: 'babel?optional[]=es7.decorators&optional[]=es7.classProperties'
            }
        ]
    },

    plugins: plugins,

    devtool: env === 'production' ? 'source-map' : 'eval'
};
