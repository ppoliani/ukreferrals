var multer = require('multer');

function registerRoutes(app){
    var router = app.loopback.Router();

    router.get('/', function(req, res) {
        res.render('index.ejs', { dev: process.env.NODE_ENV !== 'production' });
    });

    router.get('/admin', function(req, res) {
        res.render('admin.index.ejs', { dev: process.env.NODE_ENV !== 'production' });
    });

    router.post('/cvs/upload', multer({ storage: multer.memoryStorage()}).single('cv'), function(req, res) {
        var CV = app.models.Cv;
        var file = req.file;

        var cv = {
            filename: file.originalname,
            contentType: file.mimetype,
            data: file.buffer
        };

        CV.create(cv, function(err,  cv) {
            if(err) res.status(500).send(err);
            else res.status(201).send(cv.id.toString())
        });

    });

    router.get('/cvs/download/:cvId', function(req, res) {
        var CV = app.models.Cv;

        CV.findById(req.params.cvId)
            .then(function(cv) {
                res.setHeader('content-type', cv.contentType);
                res.send(cv.data);
            })
            .catch(function(err) {
                res.status(500).send(err);
            });
    });

    app.post('/signup', function (req, res, next) {

        var User = app.models.user;

        var newUser = {};
        newUser.email = req.body.email.toLowerCase();
        newUser.username = req.body.username.trim();
        newUser.password = req.body.password;

        User.create(newUser, function (err, user) {
            if (err) {
                req.flash('error', err.message);
                return res.redirect('back');
            } else {
                // Passport exposes a login() function on req (also aliased as logIn())
                // that can be used to establish a login session. This function is
                // primarily used when users sign up, during which req.login() can
                // be invoked to log in the newly registered user.
                req.login(user, function (err) {
                    if (err) {
                        req.flash('error', err.message);
                        return res.redirect('back');
                    }
                    return res.redirect('/');
                });
            }
        });
    });


    // Support Html5 mode
    router.get('/*', function(req, res, next) {
        var parts = req.url.split('.');

        if(parts.length === 1 && parts[0].indexOf('auth') === -1){
            if(req.url.indexOf('admin') === -1) {
                res.render('index.ejs', { dev: process.env.NODE_ENV !== 'production' })
            }
            else {
                res.render('admin.index.ejs', { dev: process.env.NODE_ENV !== 'production' });
            }
        }
        else {
            next();
        }
    });

    app.use(router);
}

module.exports = registerRoutes;
