var concat = require('gulp-concat');
var del = require('del');
var gulp = require('gulp');
var minifyCSS = require('gulp-minify-css');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var util = require('gulp-util');
var webpack = require('webpack');
var webpackConfig = require('./webpack.config');

// region config

var vendorScripts = [
    'node_modules/angular/angular.js',
    'node_modules/angular-ui-router/release/angular-ui-router.js',
    'node_modules/immutable/dist/immutable.js',
    'node_modules/immutable-angular/dist/immutable-angular.js',
    'client/vendor/angular-resource/angular-resource.js',
    'client/vendor/angular-animate/angular-animate.js',
    'client/vendor/angularjs-toaster/toaster.js',
    'client/vendor/angular-file-upload/dist/angular-file-upload.min.js',
    'client/vendor/modernizr.custom.79639.js',
    'client/vendor/jquery-1.11.2.min.js',
    'client/vendor/bootstrap.min.js',
    'client/vendor/retina.min.js',
    'client/vendor/scrollReveal.min.js',
    'client/vendor/jquery.flexmenu.js',
    'client/vendor/jquery.ba-cond.min.js',
    'client/vendor/jquery.slitslider.js',
    'client/vendor/owl.carousel.min.js',
    'client/vendor/parallax.js',
    'client/vendor/jquery.counterup.min.js',
    'client/vendor/waypoints.min.js',
    'client/vendor/jquery.nouislider.all.min.js',
    'client/vendor/bootstrap-wysiwyg.js',
    'client/vendor/jquery.hotkeys.js',
    'client/vendor/jflickrfeed.min.js',
    'client/vendor/fancybox.pack.js',
    'client/vendor/magic.js',
    'client/vendor/instafeed.js',
    'client/js/services/lb-services.js'
];

var vendorCSS = [
    'client/css/vendor/bootstrap.min.css',
    'client/css/vendor/jquery.flexmenu.css',
    'client/css/vendor/owl.carousel.css',
    'client/css/vendor/animate.css',
    'client/css/vendor/font-awesome.min.css',
    'client/css/vendor/jquery.fancybox.css',
    'client/css/vendor/jquery.nouislider.css',
    'client/css/vendor/bannerscollection_zoominout.css',
    'client/vendor/angularjs-toaster/toaster.css',
    'client/css/vendor/style.css'
];

var stylesDist = 'client/dist/styles/';

// endregion

gulp.task('clean', function (next) {
    del([
        'client/dist',
        'client/styles/dist'
    ], {force: true}, next);
});

gulp.task('sass', function () {
    gulp.src('./client/css/app.scss')
        .pipe(sourcemaps.init())
        .pipe(sass().on('error', sass.logError))
        .pipe(sourcemaps.write(stylesDist))
        .pipe(concat("app.min.css"))
        .pipe(minifyCSS({keepBreaks: true}))
        .pipe(gulp.dest(stylesDist));
});

gulp.task('copy:fonts', function () {
    gulp.src(['./client/css/fonts/**/*.*'])
        .pipe(gulp.dest('client/dist/fonts'));
});

gulp.task('cssmin:vendor', function () {
    gulp.src(vendorCSS)
        .pipe(concat('vendor.min.css'))
        .pipe(minifyCSS({keepBreaks: true}))
        .pipe(gulp.dest(stylesDist));
});

gulp.task('concat:vendor', function () {
    return gulp.src(vendorScripts)
        .pipe(concat('vendor.min.js'))
        //.pipe(uglify())
        .pipe(gulp.dest('client/dist'));
});

gulp.task('webpack', function (next) {
    webpack(webpackConfig, function (err, stats) {
        if (err) {
            throw new util.PluginError('webpack', err);
        }

        util.log('[webpack]', stats.toString({
            chunks: false,
            chunkModules: false,
            colors: true,
            modules: false
        }));

        next();
    });
});

gulp.task('default', ['clean'], function () {
    gulp.start(['sass', 'copy:fonts', 'concat:vendor', 'webpack', 'cssmin:vendor']);
});
