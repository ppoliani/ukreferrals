var nodemailer = require('nodemailer');

module.exports = function(Message) {

    // region Inner Methods

    function sendEmail(to, content, clb){
        var
            transporter = nodemailer.createTransport({
                service: 'Gmail',
                auth: {
                    user: 'ppoliani@gmail.com',
                    pass: 'boliev_30'
                }
            }),

            mailOptions = {
                from: 'ppoliani@gmail.com <ppoliani@gmail.com>', // sender address
                to: to,
                subject: 'uk-referrals.com',
                text: content
            };

        transporter.sendMail(mailOptions, function(err, info){
            if(err){
                clb(err);
            }
            else{
                clb(null, info.response);
            }
        });
    }

    // endregion

    // region Remote Methods

    Message.answer = function answer(req, res){
        sendEmail('ppoliani@gmail.com', req.body.content + '\n' + req.body.email, function(err) {
            if (err) {
                res.status(500).send(err);
            }
            else {
                res.status(204).send();
            }
        });
    };

    Message.remoteMethod('answer', {
        accepts: [
            { arg: 'data', type: 'object', http: { source: 'req' } },
            { arg: 'res', type: 'object', http: function(ctx){ return ctx.res; } } // get the res instance as a parameter; Couldn't find a better way
        ],
        returns: { type: 'buffer', root: true },
        http: { path:'/answer', verb: 'post' }
    });

    // endregion

};
