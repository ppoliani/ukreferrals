module.exports = {
    options: {
        separator: ';'
    },

    distVendor:{
        src:[
            'vendor/jquery/jquery.min.js',
            'vendor/angular/angular.js',
            'vendor/angular/**/*.js',
            'vendor/moment/moment.js',
            'vendor/modules/angularjs-toaster/toaster.js',
            'third-party/angular-loading-bar/build/loading-bar.js',
            'third-party/angular-datepicker/dist/index.min.js'
        ],
        dest:'dist/distVendor.js'
    },

    distApp:{
        src:[
            'js/*.js',
            'js/directives/*.js',
            'js/directives/*/**.js',
            'js/services/*.js',
            'js/filters/*.js',
            'js/controllers/*.js',
            'js/controllers/bootstrap.js'
        ],
        dest:'dist/distApp.js'
    },

    distVendorPrint: {
        src:[
            'vendor/angular/angular.js',
            'vendor/angular/angular-resource/angular-resource.js',
            'third-party/accounting/accounting.js'
        ],
        dest:'dist/vendor.print.min.js'
    },

    distAppPrint:{
        src:[
            'js/lb-services.js',
            'js/app-print.js',
            'js/services/constants.js',
            'js/filters/price-format.js',
            'js/controllers/propertyPrintCtrl.js'
        ],
        dest:'dist/app.print.min.js'
    }
};
