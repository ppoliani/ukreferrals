module.exports = {
  dist: ['dist/*'],
  dists:[
    'client/dist/css/**',
    'client/dist/js/*.js',
    'client/dist/vendor/angular/',
    'client/dist/js/directives',
    'client/dist/js/services',
    'client/dist/js/filters',
    'client/dist/index.min.html'
  ]
};
