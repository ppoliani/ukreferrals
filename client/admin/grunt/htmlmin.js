module.exports = {
	min: {
      files: [{
          expand: true,
          cwd: 'client/tpl/',
          src: ['*.html', '**/*.html'],
          dest: 'dist/tpl/',
          ext: '.html',
          extDot: 'first'
      }]
  }
}
