module.exports = {
  dist: {
    src: [
    	'css/bootstrap.css',
    	'css/animate.css',
    	'css/font-awesome.min.css',
    	'css/simple-line-icons.css',
    	'css/font.css',
    	'css/app.css',
    	'css/custom-css.css',
    	'third-party/angular-loading-bar/build/loading-bar.css',
    	'third-party/angular-datepicker/dist/index.css',
    	'vendor/modules/angularjs-toaster/toaster.css'
    ],
    dest: 'dist/app.min.css'
  }
};
