module.exports = {
  distVendor:{
    src:[
      'dist/distVendor.js'
    ],
    dest:'dist/vendor.min.js'
  },
   distApp:{
    src:[
      'dist/distApp.js'
    ],
    dest:'dist/app.min.js'
  }
}
