module.exports = {
    dev: {
        nonull: true,
        files: [
            // Include our bower JS dependencies

            // angular
            {src: "third_party/angular/angular.js", dest: "vendor/angular/angular.js"},
            {src: "third_party/angular-animate/angular-animate.js", dest: "vendor/angular/angular-animate/angular-animate.js"},
            {src: "third_party/angular-cookies/angular-cookies.js", dest: "vendor/angular/angular-cookies/angular-cookies.js"},
            {src: "third_party/angular-resource/angular-resource.js", dest: "vendor/angular/angular-resource/angular-resource.js"},
            {src: "third_party/angular-sanitize/angular-sanitize.js", dest: "vendor/angular/angular-sanitize/angular-sanitize.js"},
            {src: "third_party/angular-touch/angular-touch.js", dest: "vendor/angular/angular-touch/angular-touch.js"},

            // bootstrap
            {src: "third_party/bootstrap/dist/css/bootstrap.css", dest: "css/bootstrap.css"},
            {src: "third_party/bootstrap/dist/js/bootstrap.js", dest: "vendor/jquery/bootstrap.js"},
            {src: "**", dest: "src/fonts", cwd: 'bower_components/bootstrap/fonts', expand : true},

            // fontawesome
            {src: "third_party/font-awesome/css/font-awesome.min.css", dest: "css/font-awesome.min.css"},
            {src: "**", dest: "src/fonts", cwd: 'bower_components/font-awesome/fonts', expand : true},

            // libs
            {src: "third_party/moment/min/moment.min.js", dest: "vendor/libs/moment.min.js"},
            {src: "third_party/screenfull/dist/screenfull.min.js", dest: "vendor/libs/screenfull.min.js"},

            // core
            {src: "third_party/angular-ui-router/release/angular-ui-router.js", dest: "vendor/angular/angular-ui-router/angular-ui-router.js"},
            {src: "third_party/angular-bootstrap/ui-bootstrap-tpls.js", dest: "vendor/angular/angular-bootstrap/ui-bootstrap-tpls.js"},
            {src: "third_party/angular-translate/angular-translate.js", dest: "vendor/angular/angular-translate/angular-translate.js"},
            {src: "third_party/angular-ui-utils/ui-utils.js", dest: "vendor/angular/angular-ui-utils/ui-utils.js"},
            {src: "third_party/ngstorage/ngStorage.js", dest: "vendor/angular/ngstorage/ngStorage.js"},
            {src: "third_party/oclazyload/dist/ocLazyLoad.js", dest: "vendor/angular/oclazyload/ocLazyLoad.js"},

            // modules for lazy load
            {src: "third_party/angular-ui-select/dist/select.min.js", dest: "vendor/modules/angular-ui-select/select.min.js"},
            {src: "third_party/angular-ui-select/dist/select.min.css", dest: "vendor/modules/angular-ui-select/select.min.css"},

            {src: "third_party/textAngular/dist/textAngular.min.js", dest: "vendor/modules/textAngular/textAngular.min.js"},
            {src: "third_party/textAngular/dist/textAngular-sanitize.min.js", dest: "vendor/modules/textAngular/textAngular-sanitize.min.js"},

            {src: "third_party/venturocket-angular-slider/build/angular-slider.min.js", dest: "vendor/modules/angular-slider/angular-slider.min.js"},

            {src: "third_party/angular-bootstrap-nav-tree/dist/abn_tree_directive.js", dest: "vendor/modules/angular-bootstrap-nav-tree/abn_tree_directive.js"},
            {src: "third_party/angular-bootstrap-nav-tree/dist/abn_tree.css", dest: "vendor/modules/angular-bootstrap-nav-tree/abn_tree.css"},

            {src: "third_party/angular-file-upload/angular-file-upload.min.js", dest: "vendor/modules/angular-file-upload/angular-file-upload.min.js"},

            {src: "third_party/ngImgCrop/compile/minified/ng-img-crop.js", dest: "vendor/modules/ngImgCrop/ng-img-crop.js"},
            {src: "third_party/ngImgCrop/compile/minified/ng-img-crop.css", dest: "vendor/modules/ngImgCrop/ng-img-crop.css"},

            // {src: "bower_components/angular-ui-calendar/src/calendar.js", dest: "vendor/modules/angular-ui-calendar/calendar.js"},

            {src: "third_party/angular-ui-map/ui-map.js", dest: "vendor/modules/angular-ui-map/ui-map.js"},

            {src: "third_party/angularjs-toaster/toaster.js", dest: "vendor/modules/angularjs-toaster/toaster.js"},
            {src: "third_party/angularjs-toaster/toaster.css", dest: "vendor/modules/angularjs-toaster/toaster.css"},

            {src: "third_party/ng-grid/build/ng-grid.min.js", dest: "vendor/modules/ng-grid/ng-grid.min.js"},
            {src: "third_party/ng-grid/ng-grid.min.css", dest: "vendor/modules/ng-grid/ng-grid.min.css"},

            {src: "third_party/videogular/videogular.min.js", dest: "vendor/modules/videogular/videogular.min.js"},
            {src: "third_party/videogular-controls/controls.min.js", dest: "vendor/modules/videogular/plugins/controls.min.js"},
            {src: "third_party/videogular-buffering/buffering.min.js", dest: "vendor/modules/videogular/plugins/buffering.min.js"},
            {src: "third_party/videogular-overlay-play/overlay-play.min.js", dest: "vendor/modules/videogular/plugins/overlay-play.min.js"},
            {src: "third_party/videogular-poster/poster.min.js", dest: "vendor/modules/videogular/plugins/poster.min.js"},
            {src: "third_party/videogular-ima-ads/ima-ads.min.js", dest: "vendor/modules/videogular/plugins/ima-ads.min.js"},


        ]
    },
    dist: {
        files: [
            {expand: true, dest: 'dist/', src:'**', cwd:'src/'},
            {dest: 'dist/index.html', src:'src/index.min.html'}
        ]
    }
};
