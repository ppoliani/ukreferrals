module.exports = {
	app: {
        files: {
          'css/app.css': [
            'css/less/app.less'
          ]
        },
        options: {
          compile: true
        }
    },
    min: {
        files: {
            'dist/css/app.min.css': [
                'css/bootstrap.css',
                'css/*.css'
            ]
        },
        options: {
            compress: true
        }
    }
}
