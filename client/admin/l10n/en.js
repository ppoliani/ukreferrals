{

"header" : {
  "navbar" : {
    "UPLOAD" : "Upload",
    "new" : {
      "NEW": "New",
      "PROJECT" : "Projects",
      "TASK" : "Task",
      "USER" : "User",
      "EMAIL" : "Email"
    },
    "NOTIFICATIONS" : "Notifications"
  }
},
"aside": {
  "nav": {
    "HEADER": "Navigation",
    "JOBS": "Jobs",
    "APPLICATIONS": "Applications",
    "USER_QUESTIONS": "User Questions"
  }
}

}
