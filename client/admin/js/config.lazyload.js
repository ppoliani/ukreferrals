// lazyload config

angular.module('app')
    /**
   * jQuery plugin config use ui-jq directive , config the js and css files that required
   * key: function name of the jQuery plugin
   * value: array of the css js file located
   */
  .constant('JQ_CONFIG', {
      easyPieChart:   ['admin/vendor/jquery/charts/easypiechart/jquery.easy-pie-chart.js'],
      sparkline:      ['admin/vendor/jquery/charts/sparkline/jquery.sparkline.min.js'],
      plot:           ['admin/vendor/jquery/charts/flot/jquery.flot.min.js',
                          'admin/vendor/jquery/charts/flot/jquery.flot.resize.js',
                          'admin/vendor/jquery/charts/flot/jquery.flot.tooltip.min.js',
                          'admin/vendor/jquery/charts/flot/jquery.flot.spline.js',
                          'admin/vendor/jquery/charts/flot/jquery.flot.orderBars.js',
                          'admin/vendor/jquery/charts/flot/jquery.flot.pie.min.js'],
      slimScroll:     ['admin/vendor/jquery/slimscroll/jquery.slimscroll.min.js'],
      sortable:       ['admin/vendor/jquery/sortable/jquery.sortable.js'],
      nestable:       ['admin/vendor/jquery/nestable/jquery.nestable.js',
                          'admin/vendor/jquery/nestable/nestable.css'],
      filestyle:      ['admin/vendor/jquery/file/bootstrap-filestyle.min.js'],
      slider:         ['admin/vendor/jquery/slider/bootstrap-slider.js',
                          'admin/vendor/jquery/slider/slider.css'],
      chosen:         ['admin/vendor/jquery/chosen/chosen.jquery.min.js',
                          'admin/vendor/jquery/chosen/chosen.css'],
      TouchSpin:      ['admin/vendor/jquery/spinner/jquery.bootstrap-touchspin.min.js',
                          'admin/vendor/jquery/spinner/jquery.bootstrap-touchspin.css'],
      wysiwyg:        ['admin/vendor/jquery/wysiwyg/bootstrap-wysiwyg.js',
                          'admin/vendor/jquery/wysiwyg/jquery.hotkeys.js'],
      dataTable:      ['admin/vendor/jquery/datatables/jquery.dataTables.min.js',
                          'admin/vendor/jquery/datatables/dataTables.bootstrap.js',
                          'admin/vendor/jquery/datatables/dataTables.bootstrap.css'],
      vectorMap:      ['admin/vendor/jquery/jvectormap/jquery-jvectormap.min.js',
                          'admin/vendor/jquery/jvectormap/jquery-jvectormap-world-mill-en.js',
                          'admin/vendor/jquery/jvectormap/jquery-jvectormap-us-aea-en.js',
                          'admin/vendor/jquery/jvectormap/jquery-jvectormap.css'],
      footable:       ['admin/vendor/jquery/footable/footable.all.min.js',
                          'admin/vendor/jquery/footable/footable.core.css']
      }
  )
  // oclazyload config
  .config(['$ocLazyLoadProvider', function($ocLazyLoadProvider) {
      // We configure ocLazyLoad to use the lib script.js as the async loader
      $ocLazyLoadProvider.config({
          debug:  false,
          events: true,
          modules: [
              {
                  name: 'ngGrid',
                  files: [
                      'admin/vendor/modules/ng-grid/ng-grid.min.js',
                      'admin/vendor/modules/ng-grid/ng-grid.min.css',
                      'admin/vendor/modules/ng-grid/theme.css'
                  ]
              },
              {
                  name: 'ui.select',
                  files: [
                      'admin/vendor/modules/angular-ui-select/select.min.js',
                      'admin/vendor/modules/angular-ui-select/select.min.css'
                  ]
              },
              {
                  name:'angularFileUpload',
                  files: [
                    'admin/vendor/modules/angular-file-upload/angular-file-upload.min.js'
                  ]
              },
              {
                  name:'ui.calendar',
                  files: ['admin/vendor/modules/angular-ui-calendar/calendar.js']
              },
              {
                  name: 'ngImgCrop',
                  files: [
                      'admin/vendor/modules/ngImgCrop/ng-img-crop.js',
                      'admin/vendor/modules/ngImgCrop/ng-img-crop.css'
                  ]
              },
              {
                  name: 'angularBootstrapNavTree',
                  files: [
                      'admin/vendor/modules/angular-bootstrap-nav-tree/abn_tree_directive.js',
                      'admin/vendor/modules/angular-bootstrap-nav-tree/abn_tree.css'
                  ]
              },
              {
                  name: 'toaster',
                  files: [
                      'admin/vendor/modules/angularjs-toaster/toaster.js',
                      'admin/vendor/modules/angularjs-toaster/toaster.css'
                  ]
              },
              {
                  name: 'textAngular',
                  files: [
                      'admin/vendor/modules/textAngular/textAngular-sanitize.min.js',
                      'admin/vendor/modules/textAngular/textAngular.min.js'
                  ]
              },
              {
                  name: 'vr.directives.slider',
                  files: [
                      'admin/vendor/modules/angular-slider/angular-slider.min.js',
                      'admin/vendor/modules/angular-slider/angular-slider.css'
                  ]
              },
              {
                  name: 'com.2fdevs.videogular',
                  files: [
                      'admin/vendor/modules/videogular/videogular.min.js'
                  ]
              },
              {
                  name: 'com.2fdevs.videogular.plugins.controls',
                  files: [
                      'admin/vendor/modules/videogular/plugins/controls.min.js'
                  ]
              },
              {
                  name: 'com.2fdevs.videogular.plugins.buffering',
                  files: [
                      'admin/vendor/modules/videogular/plugins/buffering.min.js'
                  ]
              },
              {
                  name: 'com.2fdevs.videogular.plugins.overlayplay',
                  files: [
                      'admin/vendor/modules/videogular/plugins/overlay-play.min.js'
                  ]
              },
              {
                  name: 'com.2fdevs.videogular.plugins.poster',
                  files: [
                      'admin/vendor/modules/videogular/plugins/poster.min.js'
                  ]
              },
              {
                  name: 'com.2fdevs.videogular.plugins.imaads',
                  files: [
                      'admin/vendor/modules/videogular/plugins/ima-ads.min.js'
                  ]
              }
          ]
      });
  }])
;
