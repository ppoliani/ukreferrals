/**
 *
 */
 (function(){
    'use strict';

    function asideCtrl(userService){
        // region Inner Fields


        // endregion

        // region Inner Methods

        function _getUsername(){
            userService.getUsername()
                .then(function (_username_) {
                    this.username = _username_;
                }.bind(this))
                .catch(function(){
                    this.username = "Unknown";
                }.bind(this));
        }

        // endregion

        // region Viewmodel

        this.userService = userService;
        this.username = null;
        this.isAuthenticated = userService.isAuthenticated();

        // endregion

        _getUsername.call(this);
    }

    angular.module('app')
        .controller('asideCtrl', ['userService', asideCtrl]);

 })();
