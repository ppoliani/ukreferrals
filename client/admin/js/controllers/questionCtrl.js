/**
 * Question controller
 */
(function(){
    'use strict';

    function questionCtrl($state, question, Message, messageService){
        // region Inner Fields


        // endregion

        // region Inner Methods

        // endregion

        // region Viewmodel

        this.question = question;

        this.answer = '';

        this.save = function save(){
            var data = {
                id: this.question.id,
                content: this.answer
            };

            Message.answer(data)
                .$promise
                .then(function(){
                    messageService.success('Answer sent', 'Your answer was successfully sent');

                    setTimeout($state.go.bind($state, 'app.unansweredQuestions'), 2000)
                })
                .catch(function(){
                    messageService.error('Answer not sent', 'Error while sending your answer; please update the page and resend your answer');
                });
        };

        this.getFormattedDate = function getFormattedData(){
            return moment(this.question.responseDate).format("dddd, MMMM Do YYYY, h:mm:ss a");
        };


        // endregion
    }

    angular.module('app').controller('questionCtrl', [
        '$state',
        'question',
        'Message',
        'messageService',
        questionCtrl
    ]);
})();
