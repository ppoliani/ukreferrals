'use strict';

/* Controllers */
// signin controller
app.controller('SigninFormController', ['$scope', 'User', '$location', 'userService', function($scope, User, $location) {
    $scope.user = {};
    $scope.authError = null;

    $scope.login = function() {
        $scope.authError = null;

        User.login({ rememberMe: true }, {email: $scope.user.email, password: $scope.user.password})
            .$promise
            .then(function() {
                var next = $location.nextAfterLogin || '/';

                $location.nextAfterLogin = null;
                $location.path(next);
            })
            .catch(function() {
                $scope.authError = 'Wrong Credentials';
            });
    };
}]);