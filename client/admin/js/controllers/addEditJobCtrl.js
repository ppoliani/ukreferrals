/**
 * Add proptery controller
 */
(function(){
    'use strict';

    function addEditJobCtrl($state, $stateParams, Job, messageService, enums){

        // region Inner Fields

        var _jobId = $stateParams.jobId;

        // endregion

        // region Inner Methods

        function _setup(){
            if(this.mode === 'edit'){
                Job.findById({ id: _jobId })
                    .$promise
                    .then(function(job){
                        this.newJob = job;
                        this.newJob.requirements = job.requirements.join('\n');
                        this.newJob.benefits = job.benefits.join('\n');
                    }.bind(this))
                    .catch(function(){
                        messageService.error('Error loading job', 'Could not load job with the id: ' + _jobId + ' Please reload the page');
                    });
            }
        }

        function _processJob() {
            this.newJob.requirements = this.newJob.requirements.split('\n');
            this.newJob.benefits = this.newJob.benefits.split('\n');

            return this.newJob;
        }

        function _createJob(){
            Job.create(_processJob.call(this))
                .$promise
                .then(function(){
                    messageService.success('Job saved', 'A new job was successfully created');

                    $state.go('app.jobs');
                })
                .catch(function(){
                    messageService.error('Job not saved', 'Error while creating a new job');
                });
        }

        function _updateJob(){
            return _processJob.call(this).$save()
                    .then(function(){
                        messageService.success('Job updated', 'The job was successfully updated');
                    })
                    .catch(function(){
                        messageService.error('Job not updated', 'Error while updating the job');
                    });
        }

        // endregion

        // region Viewmodel

        this.enums = enums;

        this.mode = _jobId ? 'edit' : 'new';

        this.newJob = {
            location: {}
        };

        this.save = function save(invalid){
            if(!invalid ){
                if(this.mode == 'new'){
                    _createJob.call(this);
                }
                else {
                    _updateJob.call(this);
                }
            }
        };

        // endregion

        _setup.call(this);
    }

    angular.module('app')
        .controller('addEditJobCtrl', [
            '$state',
            '$stateParams',
            'Job',
            'messageService',
            'enums',
            addEditJobCtrl
        ]);
})();
