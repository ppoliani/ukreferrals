/**
 * Questions controller
 */
(function(){
    'use strict';

    function unAnsweredQuestionCtrl($stateParams, recordsCount, Message, messageService){

        // region Consts

        var LIMIT = 10;

        // endregion

        // region Inner Fields


        // endregion

        // region Inner Methods

        function _load(){
            var filter = {
                where: {
                    response: null
                },
                skip: this.currentPage - 1,
                limit: LIMIT
            };

            Message.find({ filter: filter })
                .$promise
                .then(function(messages){
                    this.messages = messages;
                }.bind(this))
                .catch(function(){
                    messageService.error('Message not loaded', 'Error while fetching messages; try to reload the page');
                });
        }

        // endregion

        // region Viewmodel

        this.messages = [];

        this.pageRange = new Array(Math.ceil(recordsCount.count / LIMIT));

        this.currentPage = $stateParams.page || 1;

        this.recordsCount = recordsCount.count;

        this.getImageSrc= function getImageSrc(property){
            return IMAGE_ENDPOINT + property.mainImage + '?width=100&height=100';
        };

        this.changePage = function changePage(page){
            this.currentPage = page;
            _load.call(this);
        };

        this.getFormattedDate = function getFormattedData(date){
          return moment(date).format("dddd, MMMM Do YYYY, h:mm:ss a");
        };

        // endregion

        _load.call(this);
    }


    angular.module('app').controller('unAnsweredQuestionCtrl', [
        '$stateParams',
        'recordsCount',
        'Message',
        'messageService',
        unAnsweredQuestionCtrl
    ]);

})();
