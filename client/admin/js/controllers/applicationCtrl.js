(function(){
    'use strict';

    function applicationCtrl($stateParams, $state, $location, Application, messageService, applicationsCount) {
        var LIMIT = 10;

        function _load(){
            var filter = {
                order: 'id DESC',
                skip: (this.currentPage - 1) * LIMIT,
                limit: LIMIT
            };

            Application.find({ filter: filter })
                .$promise
                .then(function(applications){
                    this.applications = applications;
                }.bind(this))
                .catch(function(){
                    messageService.error('Applications not fetched', 'Error while fetching applications; try to reload the page');
                });
        }

        // region Viewmodel

        this.applications = [];

        this.pageRabge = new Array(Math.ceil(applicationsCount.count / LIMIT));;

        this.applicationsCount = applicationsCount.count;

        this.currentPage = $stateParams.page || 1;

        this.changePage = function changePage(page){
            this.currentPage = page;
            _load.call(this);
        };

        this.deleteApplication = function deleteApplication(id){
            Application.deleteById({ id: id })
                .$promise
                .then(function(){
                    messageService.success('Application deleted success', 'The application was successfully deleted');

                    setTimeout($state.go.bind($state, $state.current, {}, {reload: true}), 2000);
                })
                .catch(function(){
                    messageService.error('Application not deleted', 'The application was not deleted; Please try again');
                });
        };

        this.download = function download(cv) {
            $location.url(`cvs/download/${cv}`);
        };

        // endregion

        _load.call(this);
    }

    angular.module('app')
        .controller('applicationCtrl', [
            '$stateParams',
            '$state',
            '$location',
            'Application',
            'messageService',
            'applicationsCount',
            applicationCtrl
        ]);
})();
