/**
 * Property controller
 */
(function(){
    'use strict';

    function jobsCtrl($state, $stateParams, Job, messageService, jobsCount){

        // region Consts

        var LIMIT = 10;

        // endregion

        // region Inner Fields

        // endregion

        // region Inner Methods

        function _load(){
            var filter = {
                order: 'id DESC',
                skip: (this.currentPage - 1) * LIMIT,
                limit: LIMIT
            };

            if($state.queryFilter){
                filter = angular.extend({}, filter, $state.queryFilter || {});
                $state.queryFilter = null;
                this.jobsCount = 1;
            }

            Job.find({ filter: filter })
                .$promise
                .then(function(jobs){
                    this.jobs = jobs;
                }.bind(this))
                .catch(function(){
                    messageService.error('Jobs not fetched', 'Error while fetching jobs; try to reload the page');
                });
        }

        // endregion

        // region Viewmodel

        this.properties = [];

        this.pageRange = new Array(Math.ceil(jobsCount.count / LIMIT));

        this.currentPage = $stateParams.page || 1;

        this.jobsCount = jobsCount.count;

        this.changePage = function changePage(page){
            this.currentPage = page;
            _load.call(this);
        };

        this.deleteJob = function deleteProperty(id){
            Job.deleteById({ id: id })
                .$promise
                .then(function(){
                    messageService.success('Job deleted success', 'The job was successfully deleted');

                    setTimeout($state.go.bind($state, $state.current, {}, {reload: true}), 2000);
                })
                .catch(function(){
                    messageService.error('Job not deleted', 'The job was not deleted; Please try again');
                });
        };

        // endregion

        _load.call(this);
    }

    // region CommonJS

    angular.module('app')
        .controller('jobsCtrl', [
            '$state',
            '$stateParams',
            'Job',
            'messageService',
            'jobsCount',
            jobsCtrl
        ]);

})();
