/**
 * Image container directive
 */
(function(){
    'use strict';

    function imageContainerDirective(){

        function imageContainerCtrl(){

            function swapImages(i, j){
                var tmp = this.images[i];
                this.images[i] = this.images[j];
                this.images[j] = tmp;
            }

            this.deleteImageAdapter = function deleteImageAdapter(imgID){
                this.deleteImage().call(this.context, imgID);
            };

            this.setMainImageAdapter = function setMainImageAdapter(imgID){
                this.setMainImage().call(this.context, imgID);
            };

            this.decreaseImgPriority = function decreaseImgPriority(index){
                if(index === 0) return;

                swapImages.call(this, index, index - 1);
            };

            this.increaseImgPriority = function increaseImgPriority(index){
                if(index === this.images.length - 1) return;

                swapImages.call(this, index, index + 1);
            };
        }

        return {
            restrict: 'AE',
            templateUrl: '/admin/js/directives/image-container/image-container.html',
            scope: {
                images: '=',
                imageEndpoint: '@',
                mainImage: '=',
                context: '=',
                deleteImage: '&',
                setMainImage: '&'
            },
            controller: ['IMAGE_ENDPOINT', imageContainerCtrl],
            controllerAs: 'vm',
            bindToController: true
        };
    }

    angular.module('app')
        .directive('imageContainer', imageContainerDirective);

})();
