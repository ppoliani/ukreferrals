/**
 * Image container directive
 */
(function(){
    'use strict';

    function searchDirective(){

        function searchDirectiveCtrl($state){
            // region Setup

            angular.element('#search-text').on("keydown keypress", function (event) {
                if(event.which === 13) {
                    event.preventDefault();

                    if(this.query){
                        this.search();
                    }
                }
            }.bind(this));


            // endregion

            // region Viewmodel

            this.query = '';

            this.search = function search(){
                $state.queryFilter = {
                    where: {
                        refNumber: this.query
                    }
                };

                $state.go($state.current, {}, {reload: true})
            };

            // endregion
        }

        return {
            restrict: 'AE',
            templateUrl: '/admin/js/directives/search/search.html',
            scope: { },
            controller: ['$state', searchDirectiveCtrl],
            controllerAs: 'vm',
            bindToController: true
        };
    }

    angular.module('app')
        .directive('search', searchDirective);

})();
