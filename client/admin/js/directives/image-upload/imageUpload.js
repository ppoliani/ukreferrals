/**
 * Image upload directive
 */
(function(){
    'use strict';

    function imageUploadDirective(){

        function imageUploadCtrl($window, FileUploader){
            // region Inner Methods

            // endregion

            // region Viewmodel

            this.uploader = new FileUploader({
                url: this.imageUploadEndpoint
            });

            this.uploader.filters.push({
                name: 'customFilter',
                fn: function(item /*{File|FileLikeObject}*/, options) {
                    return this.queue.length < 10;
                }
            });

            /**
             * Wrapper around the angular-file-upload item.upload method; we pass the authorization
             * header as the latter will now use the $http service to upload the file
             * @param fileItem
             */
            this.upload = function upload(fileItem){
                fileItem.headers.authorization = $window.localStorage.$LoopBack$accessTokenId;
                fileItem.upload();
            };

            /**
             * Similar to upload but applies to all selected images
             */
            this.uploadAll = function uploadAll(){
                this.uploader.queue.map(function(fileItem){
                    fileItem.headers.authorization = $window.localStorage.$LoopBack$accessTokenId;
                });

                this.uploader.uploadAll()
            };

            this.uploader.onCompleteItem = function(fileItem, response, status, headers) {
                this.imageSaved().call(this.context, response.id);
            }.bind(this);

            // endregion
        }

        return {
            restrict: 'AE',
            templateUrl: '/admin/js/directives/image-upload/image-upload.html',
            scope: {
                imageUploadEndpoint: '@',
                imageSaved: '&',
                context: '='
            },
            controller: [
                '$window',
                'FileUploader',
                imageUploadCtrl
            ],
            controllerAs: 'vm',
            bindToController: true
        };
    }

    angular.module('app')
        .directive('imageUpload', imageUploadDirective);
})();
