/**
 *
 */
(function(){
    'use strict';

    var enums = {
        contractTypes: {
            contract: {
                label: 'Contract',
                value: 'Contract'
            },

            permanent: {
                label: 'Permanent',
                value: 'Permanent'
            }
        },

        categories: {
            softwareEngineer: {
                label: 'Software Engineer',
                value: 'Software Engineer'
            },

            engineer: {
                label: 'Engineer',
                value: 'Engineer'
            }
        }
    };

    angular.module('app')
        .value('enums', enums);
})();
