(function(){
    'use strict';

    function serviceLocation($q, Town){

        var _locations = null;

        function getLocations(){
            return $q(function(resolve, reject){
                if(_locations){
                    return resolve(_locations);
                }
                else {
                    _locations = {};

                    Town.find()
                        .$promise
                        .then(function(locations){
                            locations.forEach(function(location){
                                _locations[location.name] = {
                                    label: location.name,
                                    towns: location.villages
                                };
                            });

                            resolve(_locations);
                        })
                        .catch(reject);
                }
            });
        }

        return {
            getLocations: getLocations
        };
    }

    angular.module('app')
        .factory('locations', ['$q', 'Town', serviceLocation]);
})();