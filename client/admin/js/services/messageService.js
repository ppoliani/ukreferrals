/**
 * Message service
 */
(function(){
    'use strict';

    function messageService(toaster){

        // region Inner Methods

        function info(title, msg){
            toaster.pop('info', title, msg);
        }

        function success(title, msg){
            toaster.pop('success', title, msg);
        }

        function error(title, msg){
            toaster.pop('error', title, msg);
        }

        // endregion

        // region Public API

        return {
            info: info,
            success: success,
            error: error
        };

        // endregion
    }

    angular.module('app')
        .factory('messageService', ['toaster', messageService])

})();