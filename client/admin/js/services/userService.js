/**
 * User service
 */
(function(){
    'use strict';

    function userService($state, User){

        // region Inner Methods

        function getUsername(){
            return User.getCurrent()
                .$promise
                .then(function(response){
                    return response.username;
                });
        }

        function isAuthenticated(){
            return User.isAuthenticated();
        }

        function logout(){
            User.logout()
                .$promise
                .then(function(){
                    $state.go('access.signin');
                });
        }

        // endregion

        // region Public API

        return {
            isAuthenticated: isAuthenticated,
            getUsername: getUsername,
            logout: logout
        };

        // endregion
    }

    angular.module('app')
        .factory('userService', ['$state', 'User', userService]);

})();