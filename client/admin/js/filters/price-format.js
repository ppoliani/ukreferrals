(function(angular){
    'use strict';

    function priceFilter(){
        return function(value, hideSymbol){
            return hideSymbol
                ? accounting.formatMoney(value, '', 0, '.', '.')
                : accounting.formatMoney(value, '€', 0, '.', '.');
        };
    }

    angular.module('app').filter('priceFormat', [priceFilter]);
})(angular);