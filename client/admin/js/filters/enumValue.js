(function (angular) {
    "use strict";

    angular.module("app").filter("enumValue", ["_", function (_) {
        return function (item, enumType) {
            for(var key in enumType){
                if(enumType[key].value === item){
                    return enumType[key] ? enumType[key].label : "";
                }
            }
        };
    }]);

})(window.angular = window.angular || {});
