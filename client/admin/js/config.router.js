'use strict';

/**
 * Config for the router
 */
angular.module('app')
  .run(
    [          '$rootScope', '$state', '$stateParams',
      function ($rootScope,   $state,   $stateParams) {
          $rootScope.$state = $state;
          $rootScope.$stateParams = $stateParams;
      }
    ]
  )
  .config(
    ['$stateProvider', '$urlRouterProvider', '$locationProvider',
      function ($stateProvider,   $urlRouterProvider, $locationProvider) {

          $urlRouterProvider
              .otherwise('admin/jobs/');
          $locationProvider.html5Mode(true);

          $stateProvider
              .state('app', {
                  abstract: true,
                  url: '/admin',
                  templateUrl: 'admin/tpl/app.html'
              })
              .state('app.jobs', {
                  url: '/jobs/',
                  templateUrl: 'admin/tpl/jobs.html',
                  controller: 'jobsCtrl',
                  controllerAs: 'jobsCtrl',
                  resolve: {
                      jobsCount: ['Job', function(Job){
                          return Job.count().$promise;
                      }]
                  }
              })
              .state('app.addJob', {
                  url: '/jobs/new',
                  templateUrl: 'admin/tpl/add-edit-job.html',
                  controller: 'addEditJobCtrl',
                  controllerAs: 'addEditJobCtrl'
              })
              .state('app.editJob', {
                  url: '/jobs/:jobId',
                  templateUrl: 'admin/tpl/add-edit-job.html',
                  controller: 'addEditJobCtrl',
                  controllerAs: 'addEditJobCtrl'
              })

              .state('app.messages', {
                  url: '/answers/:id',
                  templateUrl: 'admin/tpl/messages.html',
                  controller: 'messagesCtrl',
                  controllerAs: 'vm',
                  resolve: {
                      question: ['$stateParams', 'Message', function ($stateParams, Message) {
                          return Message.findById({ id: $stateParams.id  }).$promise;
                      }]
                  }
              })

              .state('app.applications', {
                  url: '/applications',
                  templateUrl: 'admin/tpl/applications.html',
                  controller: 'applicationCtrl',
                  controllerAs: 'applicationCtrl',
                  resolve: {
                      applicationsCount: ['Application', function (Application) {
                          return Application.count().$promise;
                      }]
                  }
              })

              .state('access', {
                  url: '/access',
                  template: '<div ui-view class="fade-in-right-big smooth"></div>'
              })

              .state('access.signin', {
                  url: '/signin',
                  templateUrl: 'admin/tpl/signin.html',
                  resolve: {
                      deps: ['uiLoad',
                          function( uiLoad ){
                              return uiLoad.load( ['admin/js/controllers/signin.js'] );
                          }]
                  }
              })
              .state('access.signup', {
                  url: '/signup',
                  templateUrl: 'admin/tpl/signup.html',
                  resolve: {
                      deps: ['uiLoad',
                          function( uiLoad ){
                              return uiLoad.load( ['js/controllers/signup.js'] );
                          }]
                  }
              })
              .state('access.forgotpwd', {
                  url: '/forgotpwd',
                  templateUrl: 'admin/tpl/forgotpwd.html'
              })

      }
    ]
  );
