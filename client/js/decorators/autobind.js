/**
 * @param {Object} target
 * @param {String} name
 * @param {Object} descriptor
 */
export default function autobind(target, name, descriptor) {
    const fn = descriptor.value;
    delete descriptor.value;
    delete descriptor.writable;
    descriptor.get = function() {
        return fn.bind(this);
    };
};
