import AppHeader from '../components/app-header/appHeader';
import AppFooter from '../components/app-footer/appFooter';
import ContactUs from '../components/contact-us/contactUs';
import LatestNews from '../components/latest-news/latestNews';
import RecentJobs from '../components/recent-jobs/recentJobs';
import Login from '../components/login/login';
import Register from '../components/register/register';
import NavBar from '../components/nav-bar/navBar';
import ProfilePage from '../components/profile-page/profilePage';
import Slides from '../components/slides/slides';
import JobDetailsPage from '../components/job-details-page/jobDetailsPage';
import FileUploader from '../components/file-uploader/fileUploader';

export default [
    AppFooter,
    AppHeader,
    ContactUs,
    LatestNews,
    RecentJobs,
    Login,
    Register,
    NavBar,
    ProfilePage,
    Slides,
    JobDetailsPage,
    FileUploader
];
