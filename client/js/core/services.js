import settings from '../services/settings';
import MessageService from '../services/messageService';
import AuthService from '../services/authService';
import UserService from '../services/userService';
import FileUploader from '../services/fileUploader';

export default [
    settings,
    MessageService,
    AuthService,
    UserService,
    FileUploader
];
