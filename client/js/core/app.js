require('babel/polyfill');

import immutableAngular from 'immutable-angular';
import register from './register';
import routes from '../config/routes';
import routeCallbackConfig from '../config/routeCallbacks';
import controllers from './controllers';
import services from './services';
import constants from './constants';
import models from './models';
import components from './components';

const mainModule = angular.module('app.core', [
    'ui.router',
    'immutable-angular',
    'ngResource',
    'ngAnimate',
    'angularFileUpload',
    'lbServices',
    'toaster',
    'app.services',
    'app.controllers',
    'app.models',
    'app.components'
]);


angular.module('app.services', []);
angular.module('app.controllers', []);
angular.module('app.models', []);
angular.module('app.components', []);

const servicesModule = register('app.services');
const controllersModule = register('app.controllers');
const modelsModule = register('app.models');
const componentsModule = register('app.components');

controllers.forEach((controller) => controllersModule.controller(controller.name, controller.ctrl));

services
    .concat(constants)
    .forEach((service) => servicesModule[service.type](service.name, service.service));

models.forEach((model) => modelsModule.value(model.name, model.ctor));

components.forEach((component) =>  componentsModule.directive(component.name, component.component));

mainModule.run(['userService', (userService) => {
    userService.getCurrentUser();
}]);

routes.configure(mainModule);
routeCallbackConfig(mainModule);
