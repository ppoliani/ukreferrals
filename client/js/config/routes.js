/**
 * App entry point
 */
var httpProviderConfig = (function(){
    'use strict';

    // region Consts

    // This is relative to the nodeJS static files directory !!!
    var BASE_DIR = '/js/';

    // endregion

    // region Inner Methods

    /**
     * Runs the configurations
     */
    function configure(app){
        app.config(['$stateProvider',
            '$urlRouterProvider',
            '$locationProvider', ($stateProvider, $urlRouterProvider, $locationProvider) => {
                $stateProvider
                    .state('home', {
                        url: '/',
                        templateUrl: _getPath('home/index'),
                        controller: 'homeCtrl as  vm'
                    })

                    .state('login', {
                        url: '/login',
                        templateUrl: _getPath('auth/login'),
                        controller: 'authCtrl as vm'
                    })

                    .state('profile', {
                        url: '/profile',
                        template: '<section class="container"><profile-page></profile-page></section>'
                    })

                    .state('jobDetail', {
                        url: '/job/:id',
                        template: '<section class="container"><job-details-page job-detail="jobDetail"></job-details-page></section>',
                        controller: ['$scope', 'jobDetail', ($scope, jobDetail) => { $scope.jobDetail = jobDetail }],
                        controllerAs: 'vm',
                        resolve: {
                            jobDetail: ['$stateParams', 'Job', ($stateParams, Job) => {
                                return Job.findById({ id: $stateParams.id }).$promise;
                            }]
                        }
                    });

                $urlRouterProvider.otherwise('/');
                $locationProvider.html5Mode(true);
            }]);
    }

    /**
     * Return the paths relative to base dir
     *
     * @param filePath
     * @returns {string}
     * @private
     */
    function _getPath(filePath){
        return BASE_DIR + filePath + '.html';
    }

    // endregion

    // region Public API

    return {
        configure: configure
    };

    // endregion
})();

// region Exports

export default httpProviderConfig;

// endregion
