export default function configure(app){
    app.run(['$rootScope', function($rootScope){
        $rootScope.$on('$routeChangeSuccess', function(event, route){
            $rootScope.title = 'uk referrals| ' + route.title;
        });
    }]);
}
