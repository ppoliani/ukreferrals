class Register {
    constructor() {
        this.restrict = 'AE';
        this.templateUrl = '/js/components/register/register.html';
        this.scope = {};
        this.controllerAs = 'vm';
        this.bindToController = true;
    }

    /*@ngInject*/
    controller(authService) {
        this.form = {};

        this.register = function() {
            if(this.form.password !== this.form.passwordRepeat) {
                console.log("Password mismatch");
            }
            else {
                delete this.form.passwordRepeat;

                authService.register(this.form);
            }
        }
    }
}

export default {
    name: 'register',
    component: Register
};

