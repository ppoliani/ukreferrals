class LatestNews {
    constructor() {
        this.restrict = 'AE';
        this.templateUrl = '/js/components/latest-news/latest-news.html';
        this.scope = {};
        this.controllerAs = 'vm';
        this.bindToController = true;
    }

    controller() {

    }
}

export default {
    name: 'latestNews',
    component: LatestNews
};
