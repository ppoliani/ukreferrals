class AppHeader {
    constructor() {
        this.restrict = 'AE';
        this.templateUrl = '/js/components/app-header/app-header.html';
        this.scope = {};
        this.controllerAs = 'vm';
        this.bindToController = true;
    }

    /*@ngInject*/
    controller(userService) {
        userService.currentUser
            .then((currentUser) => this.currentUser = currentUser);
    }
}

export default {
    name: 'appHeader',
    component: AppHeader
};

