class ProfilePage {
    constructor() {
        this.restrict = 'AE';
        this.templateUrl = '/js/components/profile-page/profile-page.html';
        this.scope = {};
        this.controllerAs = 'vm';
        this.bindToController = true;
    }

    /*@ngInject*/
    controller(settings) {
        settings.boot();
    }
}

export default {
    name: 'profilePage',
    component: ProfilePage
};

