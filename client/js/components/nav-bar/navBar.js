class NavBar {
    constructor() {
        this.restrict = 'AE';
        this.templateUrl = '/js/components/nav-bar/nav-bar.html';
        this.scope = {};
        this.controllerAs = 'vm';
        this.bindToController = true;
    }

    /*@ngInject*/
    controller(userService) {
        this.loginOrLogoutLabel = userService.isAuthenticated() ? 'Logout' : 'Login';

        this.loginOrLogout = function loginOrLogout() {
            if(userService.isAuthenticated()) {
                userService.logout();
            }
            else {
                $(".link-login").click(function () {
                    $("#login").fadeIn(300);
                    $("body").addClass("no-scroll");
                });
            }
        };

        this.register = function register() {
            $(".link-register").click(function () {
                $("#register").fadeIn(300);
                $("body").addClass("no-scroll");
            });
        };
    }
}

export default {
    name: 'navBar',
    component: NavBar
};

