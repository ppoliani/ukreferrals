class ContactUs {
    constructor() {
        this.restrict = 'AE';
        this.templateUrl = '/js/components/contact-us/contact-us.html';
        this.scope = {};
        this.controllerAs = 'vm';
        this.bindToController = true;
    }

    /*@ngInject*/
    controller($location, Message, messageService) {
        this.form = {};

        this.send = function send(){
            this.form.createdOn = new Date();

            Message.answer(this.form)
                .$promise
                .then(function(){
                    messageService.success('Message success', 'Your question has been successfully submitted. We will soon get back to you.');

                    setTimeout($location.path.bind($location, '/'), 2000);
                })
                .catch(function(){
                    messageService.error('Answer not sent', 'Error while sending your answer; please update the page and resend your answer');
                });
        };
    }
}

export default {
    name: 'contactUs',
    component: ContactUs
};
