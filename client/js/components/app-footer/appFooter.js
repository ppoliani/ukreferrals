class AppFooter {
    constructor() {
        this.restrict = 'AE';
        this.templateUrl = '/js/components/app-footer/app-footer.html';
        this.scope = {};
        this.controllerAs = 'vm';
        this.bindToController = true;
    }

    controller() {

    }
}

export default {
    name: 'appFooter',
    component: AppFooter
};

