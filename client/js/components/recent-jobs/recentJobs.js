class RecentJobs{
    constructor() {
        this.restrict = 'AE';
        this.templateUrl = '/js/components/recent-jobs/recent-jobs.html';
        this.scope = {};
        this.controllerAs = 'vm';
        this.bindToController = true;
    }

    /*@ngInject*/
    controller(Job, messageService) {
        const RECENT_JOBS = 10;

        this._load = function() {
            var filter = {
                order: 'id DESC',
                limit: RECENT_JOBS
            };

            Job.find({ filter })
                .$promise
                .then((recentJobs) => this.recentJobs = recentJobs)
                .catch(() => messageService.error('Could not load recent jobs'));
        };

        this._load();
    }
}

export default {
    name: 'recentJobs',
    component: RecentJobs
};

