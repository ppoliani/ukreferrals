class JobDetailsPage {
    constructor() {
        this.restrict = 'AE';
        this.templateUrl = '/js/components/job-details-page/job-details-page.html';
        this.scope = {
            jobDetail: "="
        };
        this.controllerAs = 'vm';
        this.bindToController = true;
    }

    /*ngInject*/
    controller($sce, $stateParams, settings, userService, Application, fileUploader, messageService) {
        const APPLICATION_ENDPOINT = 'cvs/upload';
        const FILENAME = 'cv';

        settings.initMenu();

        this.jobDescription = $sce.trustAsHtml(this.jobDetail.description);

        this.form = {};

        this.userService = userService;

        this.uploader = fileUploader.createUploader(APPLICATION_ENDPOINT, FILENAME);

        function _getUser() {
            return this.userService.isAuthenticated()
                ? this.userService.currentUser
                : new Promise((resolve) => resolve(this.form));
        }

        this.applyCV = function applyCV() {
            const file = this.uploader.queue[0];

            file.upload();
        };

        this.uploader.onCompleteItem = function onCompleteItem(fileItem, response, status, headers) {
            if(status !== 500) {
                _getUser.call(this)
                    .then((user) => {
                        Application.create({
                            userId: user.id,
                            cv: response, // it contains the cv id
                            jobId: $stateParams.id,
                            name: user.name,
                            email: user.email,
                            telephone: user.telephone
                        })
                        .$promise
                        .then(() => messageService.success('You application has been submitted'))
                        .catch(() => messageService.error('Could not send you application. PLease refresh the page and try again'));
                    });
            }
        }.bind(this);
    }
}

export default {
    name: 'jobDetailsPage',
    component: JobDetailsPage
};

