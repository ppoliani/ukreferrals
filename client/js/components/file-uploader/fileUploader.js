import autobind from '../../decorators/autobind';

class FileUploader {
    constructor() {
        this.restrict = 'AE';
        this.templateUrl = '/js/components/file-uploader/file-uploader.html';
        this.scope = {
            uploader: '='
        };
        this.controllerAs = 'vm';
        this.bindToController = true;
    }

    controller() {
    }
}

export default {
    name: 'fileUploader',
    component: FileUploader
};

