class Login {
    constructor() {
        this.restrict = 'AE';
        this.templateUrl = '/js/components/login/login.html';
        this.scope = {};
        this.controllerAs = 'vm';
        this.bindToController = true;
    }

    /*@ngInject*/
    controller(authService) {
        this.form = {};

        this.login = function() {
            authService.login(this.form);
        }
    }
}

export default {
    name: 'login',
    component: Login
};

