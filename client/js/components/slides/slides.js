class Slides {
    /*@ngInject*/
    constructor(settings) {
        this.restrict = 'AE';
        this.templateUrl = '/js/components/slides/slides.html';
        this.scope = {};

        this.settings = settings;
    }


    link() {
        this.settings.initSlider();
    }
}

Slides.$inject = ['settings'];

export default {
    name: 'slides',
    component: Slides
};

