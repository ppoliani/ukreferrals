class HomeCtrl {
    /*@ngInject*/
    constructor(settings) {
        settings.initMenu();
    }
}

export default {
    name: "homeCtrl",
    ctrl: HomeCtrl
}
