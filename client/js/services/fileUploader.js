class FileUploader {
    constructor(FileUploader) {
        this.FileUploader = FileUploader;
    }

    createUploader(uploadEndpoint, fileName) {
        return new this.FileUploader({
            url: uploadEndpoint,
            alias: fileName
        })
    }
}

FileUploader.$inject = ['FileUploader'];

export default {
    name: 'fileUploader',
    type: 'factory',
    service: FileUploader
}
