import autobind from '../decorators/autobind';

class AuthService {
    constructor($window, User, messageService) {
        this.$window = $window;
        this.User = User;
        this.messageService = messageService;
    }

    register(data) {
        this.User.create(data)
            .$promise
            .then(this.reload)
            .catch(() => this.messageService.error('Cannot register a new user. Please refresh the page and try again.'));
    }

    login(data) {
        this.User.login(data)
            .$promise
            .then(this.reload);
    }

    @autobind
    reload() {
        this.$window.location.reload()
    }
}

AuthService.$inject = ['$window', 'User', 'messageService'];

export default {
    name: 'authService',
    type: 'factory',
    service: AuthService
}
