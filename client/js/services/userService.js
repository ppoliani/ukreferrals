class UserService {
    constructor($window, User) {
        this.$window = $window;
        this.User = User;

        this._currentUser = null;
    }

    getCurrentUser() {
        return this.User.getCurrent()
            .$promise
            .then((user) => {
                this._currentUser = user;
            });
    }

    get currentUser() {
        return new Promise((resolve, reject) => {
            if(this._currentUser) {
                resolve(this._currentUser);
            }
            else {
                return this.getCurrentUser();
            }
        });
    }

    get userName() {
        return this._currentUser ? this._currentUser.username : null;
    }

    isAuthenticated() {
        return this.User.isAuthenticated();
    }

    logout() {
        this.User.logout()
            .$promise
            .then(() => this.$window.location.reload());
    }
}

UserService.$inject = ['$window', 'User'];

export default {
    name: 'userService',
    type: 'factory',
    service: UserService
}
