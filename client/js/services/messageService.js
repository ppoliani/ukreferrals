class MessageService {
    constructor(toaster) {
        this.toaster = toaster;
    }

    info(title, msg){
        this.toaster.pop('info', title, msg);
    }

    success(title, msg){
        this.toaster.pop('success', title, msg);
    }

    error(title, msg){
        this.toaster.pop('error', title, msg);
    }
}

// ngInject annotation didn't work
MessageService.$inject = ['toaster'];

export default {
    name: 'messageService',
    type: 'factory',
    service: MessageService
};
